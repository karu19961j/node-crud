/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const app = require('express')();

const apiCRUD = async (APIRoutes) => {
  APIRoutes.forEach((api) => {
    api.methods.forEach((method) => {
      app[method](`/api/${api.route}`, require(`./routes/${api.controller}`));
    });
  });
};


module.exports.apiCRUD = apiCRUD;
