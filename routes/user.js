const router = require('express').Router();
const userController = require('../controllers/users.js');
const responseHelper = require('../helpers/response');

router.post(
  '/register',
  async (req, res) => {
    try {
      const { table } = req.headers;
      await userController.register(req.body, table);
      responseHelper.success(res, 'User created successfully', 200);
    } catch (error) {
      responseHelper.failure(res, error, 500);
    }
  },
);

module.exports = router;
