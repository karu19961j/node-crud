const register = async (body, TableName) => {
  try {
    const { email } = body;
    const data = await TableName.findOne({ email });
    if (data) {
      throw Error('Email is not unique');
    }
    await new TableName(body).save();
  } catch (error) {
    throw Error(error.message);
  }
};

module.exports = {
  register,
};
